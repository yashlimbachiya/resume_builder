## TBD

Are first name and last name supposed to be different inputs? even on the google sheet?

## Validations

Email has to be validated as per norms
Number has to be validated as per norms
Date of birth has to be of format dd-mm-yyyy (Regex Daalo)
start date has to be of format MM-YYYY
end date has to be of format MM-YYYY

## Scribles

<!-- 4 internships
Post held in campus max 4 -->

extracurricular word limit 50

Fields Required:

    First Name: text
    Last Name: text

    About Me: Textarea

    Personal Details: FormGroup = {
    	Address: textarea
    	mobile: number
    	home?: number
    	email: email
    	dateofbirth: date
    	languages: chips
    	hobbies: text
    }

    Certifications: FormArray = [
    	certification: text
    ]

    Education: FormArray = [
    	Institute: FormGroup = {
    		instituteName: text
    		startYear: number
    		endYear: number
    		courseName: text
    		description: textarea
    	}
    ]

    Experiences: FormArray = [
    	Organization: FormGroup = {
    		orgName: text
    		startYear: number
    		endYear: number
    		roleOrDesignation: text
    		description: textarea
    	}
    ]

    Internships: FormArray = [
    	Organization: FormGroup = {
    		orgName: text
    		startYear: number
    		endYear: number
    		roleOrDesignation: text
    		description: textarea
    	}
    ]
