/*
 * pre-commit-angular-base
 * Copyright (C) 2019 iSchoolConnect
 * mailto:support AT ischoolconnect DOT com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { GetResumeDialogComponent } from "./get-resume-dialog.component";

describe("GetResumeDialogComponent", () => {
    let component: GetResumeDialogComponent;
    let fixture: ComponentFixture<GetResumeDialogComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [GetResumeDialogComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(GetResumeDialogComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});
