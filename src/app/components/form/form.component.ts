/*
 * pre-commit-angular-base
 * Copyright (C) 2019 iSchoolConnect
 * mailto:support AT ischoolconnect DOT com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
import { Component, OnInit, ViewChild, ViewChildren } from "@angular/core";
import { FormBuilder, FormControl, FormArray, FormGroup, Validators } from "@angular/forms";
import { MatStepper } from "@angular/material";
import { ApiService } from "src/app/services/api.service";

// For date stuff

@Component({
    selector: "app-form",
    templateUrl: "./form.component.html",
    styleUrls: ["./form.component.scss"],
})
export class FormComponent implements OnInit {
    @ViewChild("stepper", { static: true }) stepper: MatStepper;

    // Form groups & Arrays
    public resumeBuilder: FormGroup;

    // main form
    public personalDetails: FormGroup;

    public internships: FormArray;

    public education: FormArray;

    public projects: FormArray;

    public extraCurricular: FormArray;

    public certifications: FormControl = new FormControl(null);

    // Form Controls
    public institutes: string[] = ["Institute 1", "Institute 2", "Institute 3", "Institute 4", "Institute 5"];

    constructor(private _formBuilder: FormBuilder, private _api: ApiService) {
        // return;
    }

    ngOnInit(): void {
        this.initializeForm();
    }

    initializeForm(): void {
        this.personalDetails = this._formBuilder.group({
            first_name: new FormControl(null, [Validators.required, Validators.pattern(/[a-zA-Z]/g)]),
            last_name: new FormControl(null, [Validators.required, Validators.pattern(/[a-zA-Z]/g)]),
            email: new FormControl(null, [Validators.required, Validators.email]),
            mobile_number: new FormControl(null, [Validators.required, Validators.pattern(/[\d+ -]+/g)]),
            home_number: new FormControl(null, [Validators.pattern(/[\d+ -]+/g)]),
            date_of_birth: new FormControl(null, [Validators.required]),
            address: new FormControl(null, [Validators.required]),
            languages: new FormControl(null, [Validators.required, Validators.pattern(/[a-zA-Z]/g)]),
            hobbies: new FormControl(null),
        });
        this.education = this._formBuilder.array([this.createEducation()]);
        this.internships = this._formBuilder.array([this.createInternship()]);
        this.projects = this._formBuilder.array([this.createProject()]);
        this.extraCurricular = this._formBuilder.array([this.createExtraCurricular()]);

        this.resumeBuilder = this._formBuilder.group({
            personal_details: this.personalDetails,
            certifications: this.certifications,
            education: this.education,
            extra_curricular: this.extraCurricular,
            internships: this.internships,
            projects: this.projects,
        });
    }

    // Internship creation, addition, removal
    private createInternship(): FormGroup {
        return this._formBuilder.group({
            org_name: new FormControl(null, [Validators.required]),
            start_year: new FormControl(null, [Validators.required]),
            end_year: new FormControl(null, [Validators.required]),
            role_or_designation: new FormControl(null, [Validators.required]),
            description: new FormControl(null),
        });
    }

    private addNewInternship(): void {
        // this.internships = this.resumeBuilder.get('internships') as Form
        this.internships.push(this.createInternship());
    }

    private removeInternship(index: number): void {
        this.internships.removeAt(index);
    }

    // ExtraCurricular creation, addition, removal
    private createExtraCurricular(): FormGroup {
        return this._formBuilder.group({});
    }

    private addNewExtraCurricular(): void {
        this.extraCurricular.push(this.createExtraCurricular());
    }

    private removeExtraCurricular(index: number): void {
        this.extraCurricular.removeAt(index);
    }

    // Education creation, addition, removal
    private createEducation(): FormGroup {
        return this._formBuilder.group({
            institute_name: new FormControl(null),
            start_year: new FormControl(null),
            end_year: new FormControl(null),
            course_name: new FormControl(null),
            description: new FormControl(null),
        });
    }

    private addNewEducation(): void {
        this.education.push(this.createEducation());
    }

    private removeEducation(index: number): void {
        this.education.removeAt(index);
    }

    // Project creation, addition, removal
    private createProject(): FormGroup {
        return this._formBuilder.group({
            project_title: new FormControl("", [Validators.required]),
            project_description: new FormControl("", [Validators.required]),
        });
    }

    private addNewProject(): void {
        this.projects.push(this.createProject());
    }

    private removeProject(index: number): void {
        this.projects.removeAt(index);
    }

    public submitForm(): void {
        // event.preventDefault();
        const submittedValue = this.resumeBuilder.value;
        // const date = submittedValue.personal_details.date_of_birth.date();
        // const year = submittedValue.personal_details.date_of_birth.year();
        // const month = submittedValue.personal_details.date_of_birth.month() + 1;
        // console.log(`Date is ${date}-${month}-${year}`);
        if (this.resumeBuilder.valid) {
            console.dir("value is ", submittedValue);
        } else {
            console.error("Form in invalid. plj fix.");
        }
        const requestObj = {
            first_name: submittedValue.personal_details.first_name,
            last_name: submittedValue.personal_details.last_name,
            email: submittedValue.personal_details.email,
            mobile_number: submittedValue.personal_details.mobile_number,
        };
        this._api.sendDetailsToSheet(requestObj).subscribe((res) => {
            // console.log(res);
        });

        // form the object to send for the request
        // const resumeDetails = {
        //     full_name: submittedValue.first_name + submittedValue.last_name,
        // };
    }

    public resetForm(): void {
        // Reset Current Form
        // console.log('reseting form');

        this.resumeBuilder.reset();
        this.resumeBuilder.markAsPristine();
        this.resumeBuilder.markAsUntouched();
        this.personalDetails.reset();
        this.personalDetails.markAsPristine();
        this.personalDetails.markAsUntouched();
        // Reinitialize Form
        this.initializeForm();

        // Bring stepper to the original position
        this.stepper.reset();
        // Reset FormArrays
        // this.resumeBuilder.setControl("internships", new FormArray([this.createInternship()]));
        // this.resumeBuilder.setControl("education", new FormArray([this.createEducation()]));
        // this.resumeBuilder.setControl("extracurricular", new FormArray([this.createExtraCurricular()]));
        // this.resumeBuilder.setControl("projects", new FormArray([this.createProject()]));
    }

    // Getters

    get email() {
        return this.personalDetails.get("email");
    }

    get dateOfBirth() {
        return this.personalDetails.get("date_of_birth");
    }

    get mobileNumber() {
        return this.personalDetails.get("mobile_number");
    }

    get homeNumber() {
        return this.personalDetails.get("home_number");
    }
}
