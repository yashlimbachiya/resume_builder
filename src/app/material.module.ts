/*
 * pre-commit-angular-base
 * Copyright (C) 2019 iSchoolConnect
 * mailto:support AT ischoolconnect DOT com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
    MatInputModule,
    MatDatepickerModule,
    MatSelectModule,
    MatButtonModule,
    MatDialogModule,
    MatStepperModule,
} from "@angular/material";
import { MatMomentDateModule, MomentDateAdapter } from "@angular/material-moment-adapter";
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from "@angular/material/core";
import { STEPPER_GLOBAL_OPTIONS } from "@angular/cdk/stepper";
// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
    parse: {
        dateInput: "LL",
    },
    display: {
        dateInput: "LL",
        monthYearLabel: "MMM YYYY",
        dateA11yLabel: "LL",
        monthYearA11yLabel: "MMMM YYYY",
    },
};

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        MatInputModule,
        MatDatepickerModule,
        MatMomentDateModule,
        MatSelectModule,
        MatButtonModule,
        MatDialogModule,
        MatStepperModule,
    ],
    exports: [
        MatInputModule,
        MatDatepickerModule,
        MatMomentDateModule,
        MatSelectModule,
        MatButtonModule,
        MatDialogModule,
        MatStepperModule,
    ],
    providers: [
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },

        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
        { provide: STEPPER_GLOBAL_OPTIONS, useValue: { showError: true } },
    ],
})
export class MaterialModule {}
